//
//  UserModel.swift
//  Practica6
//
//  Created by Manuel on 02/05/19.
//  Copyright © 2019 Manuel Alejandro. All rights reserved.
//


import Foundation

class UserModel {
    var id: Int = -1
    var name: String = ""
    var username: String = ""
    var city: String = ""
    var lat: Double = 0
    var lng: Double = 0
    
    init(name: String, city: String){
        self.name = name
        self.city = city
    }
}
