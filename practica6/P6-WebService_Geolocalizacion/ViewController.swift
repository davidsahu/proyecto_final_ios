//
//  ViewController.swift
//  Practica6
//
//  Created by David Sánchez on 08/05/19.
//  Copyright © 2019 David Sánchez . All rights reserved.
//


import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usuarios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "user")
        cell?.textLabel?.text = usuarios[indexPath.row].name
        cell?.detailTextLabel?.text = usuarios[indexPath.row].city
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "detail", sender: self)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    //contenedor de Usuarios
    var usuarios = [UserModel]()
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //Inicializador usuarios
        //usuarios.append(UserModel(name:"Aldo", city: "Republica Hermana de Tonala"))
        //usuarios.append(UserModel(name:"Jose", city: "Culiacan"))
        //usuarios.append(UserModel(name:"David", city: "Uruapan"))
        //usuarios.append(UserModel(name:"Daniel", city: "Guadalajara"))
        //usuarios.append(UserModel(name:"Sergio", city: "Guadalajara"))
        //tableView.reloadData()
        
        getUser()
    }
    
    //Segue preparation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let destino = segue.destination as! DetailViewController
        destino.user = usuarios[selectedIndex]
    }
    
    func getUser(){
        loader.startAnimating()
        
        //Direccion (String)
        let urlString = "https://jsonplaceholder.typicode.com/users"
        //Generar URL
        let url = URL(string: urlString)!
        
        let session = URLSession.shared
        //Objeto request
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if (error != nil){
                return
            }
            if (data == nil){
                return
            }
            // Si no hay Error y si Exite la informacion...
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! [[String: Any?]]
                print(json)
                
                //descomponer JSON
                for user in json {
                    let name = user["name"] as! String
                    let address = user["address"] as! [String: Any]
                    let city = address["city"] as! String
                    
                    let userItem = UserModel(name: name, city: city)
                    
                    //Setera las Propiedades de Usuario
                    userItem.username = user["username"] as! String
                    let geo = address["geo"] as! [String: Any]
                    userItem.lat = Double(geo["lat"] as! String)!
                    userItem.lng = Double(geo["lng"] as! String)!
                    
                    //Llenar el arreglo de Usuarios
                    self.usuarios.append(UserModel(name: name, city: city))
                    
                }
                //Llenar areglo de Usuarios
                DispatchQueue.main.async{
                self.tableView.reloadData()
                self.loader.stopAnimating()
                }
                
            } catch let error {
                print("Error al deserializar: \(error.localizedDescription)")
            }
        }
        task.resume()
    }

    }
