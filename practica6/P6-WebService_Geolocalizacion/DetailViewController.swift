//
//  DetailViewController.swift
//  Practica6
//
//  Created by David Sánchez on 08/05/19.
//  Copyright © 2019 David Sánchez . All rights reserved.


import UIKit
import MapKit

class DetailViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var lblUser: UILabel!
    @IBOutlet weak var map: MKMapView!
    
    var user: UserModel!
    
    var manager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        
        //Inicializar UI
        lblUser.text = user.name
        print("Latitud: \(user.lat)), Longitud: \(user.lng))")
        
        let mark = MKPointAnnotation()
        mark.title = user.name
        mark.coordinate = CLLocationCoordinate2D(latitude: user.lat, longitude: user.lng)
        map.addAnnotation(mark)
        
        map.userTrackingMode = .follow
        map.showsUserLocation = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {self.map.setCenter(mark.coordinate, animated: true)
            
        }
        
        let status = CLLocationManager.authorizationStatus()
        if (status == .notDetermined || status == .denied || status == .authorizedWhenInUse)
        {
            manager.requestAlwaysAuthorization()
            manager.requestWhenInUseAuthorization()
        }
        
        manager.startUpdatingLocation()
        manager.startUpdatingHeading()
    }
   
}
