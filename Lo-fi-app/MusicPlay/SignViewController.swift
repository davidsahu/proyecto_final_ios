//
//  SignViewController.swift
//  MusicPlay
//
//  Created by David Sánchez on 5/22/19.
//

import UIKit

class SignViewController: UIViewController {

    @IBOutlet weak var entrar_button: UIButton!
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ocultarTeclado))
        view.addGestureRecognizer(tap)
        
        entrar_button.layer.cornerRadius = 10.0
        entrar_button.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func entrarButton(_ sender: Any) {
        // Constantes login
        let usuario = "davidsahu";
        let contraseña = "123"
        
        if (usuario == txtUser.text && contraseña == txtPassword.text) {
            print("Generar alerta de bienvenida")
            
            /*
            // Crear alerta
            let alerta = UIAlertController(title: "bienvenido", message: "bienvenido credenciales correctas", preferredStyle: .alert)
            
            // Agregar acciones de alerta
            alerta.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil ));
            
            alerta.addAction(UIAlertAction(title: "Otra acción", style: .default, handler: nil ));
            
            // Mostrar la alerta
            self.present(alerta, animated: true, completion: nil);
            */
            let musica = self.storyboard?.instantiateViewController(withIdentifier: "FirstViewController") as! FirstViewController
            
            self.present(musica, animated: true)
            
        } else {
            print("Generar alerta de error")
            
            // Crear alerta
            let alerta_falsa = UIAlertController(title: "Error!", message: "Usuario o contraseña no es correcta", preferredStyle: .actionSheet)
            // Agregar acciones
            alerta_falsa.addAction(UIAlertAction(title: "Cuack", style: .destructive, handler: nil));
            
            // Mostrar alerta
            self.present(alerta_falsa, animated: true, completion: nil);
            
        }
        print("Hola Mundo");
        
    }
    
    @objc func ocultarTeclado(segundo: Int) {
        view.endEditing(true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
